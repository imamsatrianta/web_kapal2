<!DOCTYPE html>
<html lang="en">
    <?php require_once('frontend/temp_front/head.php'); ?>
    <body>
        <div id="wrapper">
            <!-- start header -->
            <?php require_once('frontend/temp_front/header.php'); ?>
            <!-- end header -->
            <!--Start Content -->
            <section id="content">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Our videos</h1>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <?php echo $contents; ?>
                    </div>
                </div>
            </section>
            <!--End Content-->
            <!-- Start Footer -->
            <?php require_once('frontend/temp_front/footer.php'); ?>
            <!-- End Footer -->
        </div>
        <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
        <!--Start js-->
        <?php require_once('frontend/temp_front/js.php'); ?>
        <!--End js-->
    </body>

</html>
