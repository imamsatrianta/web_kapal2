<!-- start slider -->			
<!-- Slider -->
<section id="featured" class="bg">
    <div id="main-slider" class="main-slider flexslider" >
        <ul class="slides">
            <li>
                <img src="<?php echo base_url(); ?>asset/front/img/slides/flexslider/1.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Modern Design</h3>
                    <p>Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urna</p>
                    <a href="#" class="btn btn-theme">Learn More</a>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>asset/front/img/slides/flexslider/2.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Fully Responsive</h3>
                    <p>Sodales neque vitae justo sollicitudin aliquet sit amet diam curabitur sed fermentum.</p>
                    <a href="#" class="btn btn-theme">Learn More</a>
                </div>
            </li>
            <li>
                <img src="<?php echo base_url(); ?>asset/front/img/slides/flexslider/3.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Clean & Fast</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit donec mer lacinia.</p>
                    <a href="#" class="btn btn-theme">Learn More</a>
                </div>
            </li>
        </ul>
    </div>
    <!-- end slider -->
    <div class="container">
        <div class="row">

        </div>
    </div>


</section>
<section class="callaction">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="cta-text">
                    <h2>welcome to the   <span>web</span> company</h2>
                    <p> Etiam adipiscing, justo quis feugiat.Suspendisse eu erat quam. Vivamus porttitor eros quis nisi lacinia sed interdum</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="cta-btn">
                    <a href="#" class="btn btn-theme btn-lg">Get Our Products &nbsp;<i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h2>We use <span class="highlight">modern</span> infrastructure & technology</h2>
                    <p>Lorem ipsum dolor sit amet, ne duis posse mei, ut cum vero nominati. Sed graece aeterno consectetuer te. Cu duo tota deleniti, vis ea fuisset nostrum. Meliore inciderint qui ne. Suas cotidieque vel ut ei eros perpetua qui. Ponderum lobortis reformidans</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="box">
                            <div class="aligncenter">
                                <div class="icon">
                                    <i class="fa fa-desktop fa-5x"></i>
                                </div>
                                <h4>Fully responsive</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="box">
                            <div class="aligncenter">
                                <div class="icon">
                                    <i class="fa fa-file-code-o fa-5x"></i>
                                </div>
                                <h4>Fully responsive</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="box">
                            <div class="aligncenter">
                                <div class="icon">
                                    <i class="fa fa-paper-plane-o fa-5x"></i>
                                </div>
                                <h4>Fully responsive</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="box">
                            <div class="aligncenter">
                                <div class="icon">
                                    <i class="fa fa-cubes fa-5x"></i>
                                </div>
                                <h4>Fully responsive</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- divider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="solidline">
                </div>
            </div>
        </div>
    </div>
    <!-- end divider -->

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-6 col-lg-6">
                        <h3>Our Goal</h3>
                        <h4>Vision</h4>
                        <span class="pullquote-left">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. </span>
                        <span class="pullquote-right margintop10">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. </span>
                        <br>
                        <h4>Mission</h4>
                        <span class="pullquote-left">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. </span>
                        <span class="pullquote-right margintop10">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. </span>

                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <h4>Projects</h4>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                40% Complete (success)
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                20% Complete
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                60% Complete (warning)
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                80% Complete
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- divider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="blankline">
                </div>
            </div>
        </div>
    </div>
    <!-- end divider -->

    <!-- parallax  -->
    <div id="parallax1" class="parallax text-light text-center marginbot50" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row appear stats">
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="align-center color-white txt-shadow">
                        <div class="icon">
                            <i class="fa fa-paper-plane-o fa-5x"></i>
                        </div>
                        <strong id="counter-coffee" class="number">1232</strong><br />
                        <span class="text">Visiteed</span>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="align-center color-white txt-shadow">
                        <div class="icon">
                            <i class="fa fa-cubes fa-5x"></i>
                        </div>
                        <strong id="counter-music" class="number">345</strong><br />
                        <span class="text">Products</span>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="align-center color-white txt-shadow">
                        <div class="icon">
                            <i class="fa fa-photo fa-5x"></i>
                        </div>
                        <strong id="counter-clock" class="number">501</strong><br />
                        <span class="text">Gallery</span>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="align-center color-white txt-shadow">
                        <div class="icon">
                            <i class="fa fa-trophy fa-5x"></i>
                        </div>
                        <strong id="counter-heart" class="number">378</strong><br />
                        <span class="text">Achivment</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <h4>Testimonials</h4>
                        <div class="testimonialslide clearfix flexslider">
                            <ul class="slides">
                                <li>
                                    <blockquote>
                                        Usu ei porro deleniti similique, per no consetetur necessitatibus. Ut sed augue docendi alienum, ex oblique scaevola inciderint pri, unum movet cu cum. Et cum impedit epicuri
                                    </blockquote>
                                    <h4>Daniel Dan <span>&#8213; MA System</span></h4>
                                </li>
                                <li>
                                    <blockquote>
                                        Usu ei porro deleniti similique, per no consetetur necessitatibus. Ut sed augue docendi alienum, ex oblique scaevola inciderint pri, unum movet cu cum. Et cum impedit epicuri
                                    </blockquote>
                                    <h4>Mark Wellbeck <span>&#8213; AC Software </span></h4>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <h3>Trending</h3>
                        <div class="tab-pane active" id="one">
                            <a href="#"><h5>Cares For Lombok</h5></a>
                            <p><img src="<?php echo base_url(); ?>asset/front/img/dummy1.jpg" class="pull-left" alt="" />
                                <strong>Augue iriure</strong> dolorum per ex, ne iisque ornatus veritus duo. Ex nobis integre lucilius sit, pri ea falli ludus appareat. Eum quodsi fuisset id, nostro patrioque qui id. Nominati eloquentiam in mea.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- divider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="solidline">
                </div>
            </div>
        </div>
    </div>
    <!-- end divider -->

    <!-- Portfolio Projects -->
    <div class="container marginbot50">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="heading">Recent Works</h4>




                <div id="grid-container" class="cbp-l-grid-projects">
                    <ul>
                        <li class="cbp-item graphic">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="<?php echo base_url(); ?>asset/front/img/works/1.jpg" alt="" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="<?php echo base_url(); ?>asset/front/img/works/1big.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Dashboard</div>
                            <div class="cbp-l-grid-projects-desc">Web Design / Graphic</div>
                        </li>
                        <li class="cbp-item web-design logo">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="<?php echo base_url(); ?>asset/front/img/works/2.jpg" alt="" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <a href="<?php echo base_url(); ?>asset/front/img/works/2big.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="World Clock Widget<br>by Paul Flavius Nechita">view larger</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">World Clock Widget</div>
                            <div class="cbp-l-grid-projects-desc">Logo / Web Design</div>
                        </li>
                        <li class="cbp-item graphic logo">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="<?php echo base_url(); ?>asset/front/img/works/3.jpg" alt="" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">

                                            <a href="http://vimeo.com/14912890" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="To-Do Dashboard<br>by Tiberiu Neamu">view video</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">To-Do Dashboard</div>
                            <div class="cbp-l-grid-projects-desc">Graphic / Logo</div>
                        </li>
                        <li class="cbp-item web-design graphic">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="<?php echo base_url(); ?>asset/front/img/works/4.jpg" alt="" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">

                                            <a href="<?php echo base_url(); ?>asset/front/img/works/4big.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Events and  More<br>by Tiberiu Neamu">view larger</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Events and More</div>
                            <div class="cbp-l-grid-projects-desc">Web Design / Graphic</div>
                        </li>
                        <li class="cbp-item identity web-design">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="<?php echo base_url(); ?>asset/front/img/works/5.jpg" alt="" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">

                                            <a href="<?php echo base_url(); ?>asset/front/img/works/5big.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="WhereTO App<br>by Tiberiu Neamu">view larger</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">WhereTO App</div>
                            <div class="cbp-l-grid-projects-desc">Web Design / Identity</div>
                        </li>
                        <li class="cbp-item identity web-design">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="<?php echo base_url(); ?>asset/front/img/works/6.jpg" alt="" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">

                                            <a href="<?php echo base_url(); ?>asset/front/img/works/6big.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Ski * Buddy<br>by Tiberiu Neamu">view larger</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Ski * Buddy</div>
                            <div class="cbp-l-grid-projects-desc">Identity / Web Design</div>
                        </li>
                        <li class="cbp-item graphic logo">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="<?php echo base_url(); ?>asset/front/img/works/7.jpg" alt="" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">

                                            <a href="<?php echo base_url(); ?>asset/front/img/works/7big.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu">view larger</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Seemple* Music for iPad</div>
                            <div class="cbp-l-grid-projects-desc">Graphic / Logo</div>
                        </li>
                        <li class="cbp-item graphic logo">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="<?php echo base_url(); ?>asset/front/img/works/8.jpg" alt="" />
                                </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">

                                            <a href="<?php echo base_url(); ?>asset/front/img/works/8big.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu">view larger</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title">Seemple* Music for iPad</div>
                            <div class="cbp-l-grid-projects-desc">Graphic / Logo</div>
                        </li>
                    </ul>
                </div>

                <div class="cbp-l-loadMore-button">
                    <a href="<?php echo base_url(); ?>asset/front/ajax/loadMore.html" class="cbp-l-loadMore-button-link">LOAD MORE</a>
                </div>

            </div>
        </div>
    </div>


    <!-- divider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="solidline">
                </div>
            </div>
        </div>
    </div>
    <!-- end divider -->

    <!-- clients -->
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-md-2 aligncenter client">
                <img alt="logo" src="<?php echo base_url(); ?>asset/front/img/clients/acs.jpg" class="img-responsive" />
            </div>

            <div class="col-xs-6 col-md-2 aligncenter client">
                <img alt="logo" src="<?php echo base_url(); ?>asset/front/img/clients/bumn.jpg" class="img-responsive" />
            </div>

            <div class="col-xs-6 col-md-2 aligncenter client">
                <img alt="logo" src="<?php echo base_url(); ?>asset/front/img/clients/darmapersada.jpg" class="img-responsive" />
            </div>

            <div class="col-xs-6 col-md-2 aligncenter client">
                <img alt="logo" src="<?php echo base_url(); ?>asset/front/img/clients/ptsabda.jpg" class="img-responsive" />
            </div>

            <div class="col-xs-6 col-md-2 aligncenter client">
                <img alt="logo" src="<?php echo base_url(); ?>asset/front/img/clients/dephub.jpg" class="img-responsive" />
            </div>
            <div class="col-xs-6 col-md-2 aligncenter client">
                <img alt="logo" src="<?php echo base_url(); ?>asset/front/img/clients/logo6.png" class="img-responsive" />
            </div>

        </div>
    </div>