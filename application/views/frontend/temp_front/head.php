<head>
    <meta charset="utf-8">
    <title>Sailor - Bootstrap 3 corporate template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Bootstrap 3 template for corporate business" />
    <!-- css -->
    <link href="<?php echo base_url(); ?>asset/front/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>asset/front/plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />
    <link href="<?php echo base_url(); ?>asset/front/css/cubeportfolio.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>asset/front/css/style.css" rel="stylesheet" />

    <!-- Theme skin -->
    <link id="t-colors" href="<?php echo base_url(); ?>asset/front/skins/default.css" rel="stylesheet" />

    <!-- boxed bg -->
    <link id="bodybg" href="<?php echo base_url(); ?>asset/front/bodybg/bg1.css" rel="stylesheet" type="text/css" />
</head>