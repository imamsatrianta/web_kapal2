<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-lg-4">
                <div class="widget">
                    <h4>About company</h4>
                    <p>
                        Vel quot idque philosophia ex. In possit ceteros accumsan vel, modus impedit <br>recusabo ne eam, ne mel wisi delenit repudiandae. Ei eum oratio patrioque instructior, cibo melius tacimates vis cu. Aeque voluptatum sit ut, pri reque labitur explicari ad, docendi democritum vix no. Quo ne 
                    </p>
                </div>
            </div>
            <div class="col-sm-4 col-lg-4">
                <div class="widget">
                    <h4>Information</h4>
                    <address>
                        <strong>Sailor company Inc</strong><br>
                        Sailor suite room V124, DB 91<br>
                        Someplace 71745 Earth </address>
                    <p>
                        <i class="fa fa-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
                        <i class="fa fa-envelope"></i> email@domainname.com
                    </p>
                </div>
            </div>
            <div class="col-sm-3 col-lg-4">
                <div class="widget">
                    <h4>Newsletter</h4>
                    <p>Fill your email and sign up for monthly newsletter to keep updated</p>
                    <div class="form-group multiple-form-group input-group">
                        <input type="email" name="email" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-theme btn-add">Subscribe</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="copyright">
                        <p>&copy; Web Kapal - All Right Reserved</p>
                        <div class="credits">
                            Designed by <a href="https://bootstrapmade.com/">Team Alpha .PJ</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <ul class="social-network">
                        <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>