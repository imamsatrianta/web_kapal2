<header>
<!--    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="topleft-info">
                        <li><i class="fa fa-phone"></i> +62 088 999 123</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">
                            <input class="sb-search-submit" type="submit" value="">
                            <span class="sb-icon-search" title="Click to start searching"></span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
<div class="trapesium"><a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>asset/front/img/logo1.jpg" style="-moz-transform: scale(1, -1);-webkit-transform: scale(1, -1);-o-transform: scale(1, -1);-ms-transform: scale(1, -1);transform: scale(1, -1);position: fixed;"/></a></div>
<div class="navbar-fixed-top" style="background-color:rgba(22, 34, 206, 0.8); top: -35px;">
    <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar" style="background-color:#FFF;"></span>
                    <span class="icon-bar" style="background-color:#FFF;"></span>
                    <span class="icon-bar" style="background-color:#FFF;"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">About US <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown">Company Profile</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url();?>index.php/home/visi_misi">Vision and Mission</a></li>
                                    <li><a href="<?php echo base_url();?>index.php/home/moto">Company Moto</a></li>
                                    <li><a href="<?php echo base_url();?>index.php/home/history">History</a></li>
                                    <li><a href="<?php echo base_url();?>index.php/home/contact">Contact</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo base_url();?>index.php/home/achivment">Achivment</a></li>
                            <li><a href="<?php echo base_url();?>index.php/home/quality_policy">Quality Policy</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url();?>index.php/home/our_product">Our Product</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Gallery <i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url();?>index.php/home/our_photos">Our Photos</a></li>
                            <li><a href="<?php echo base_url();?>index.php/home/our_videos">Our Videos</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url();?>index.php/home/career">Career</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>