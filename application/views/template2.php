<!DOCTYPE html>
<html lang="en">
    <?php require_once('frontend/temp_front/head.php'); ?>
    <body>
        <div id="wrapper">
            <!-- start header -->
            <?php require_once('frontend/temp_front/header.php'); ?>
            <!-- end header -->
            <!--Start Content -->
            <section id="content" style="padding: 115px 0px 20px">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <article>
                                <?php echo $contents; ?>
                            </article>
                        </div>
                        <!-- Start Sidebar-->
                        <?php require_once('frontend/temp_front/sidebar.php'); ?>
                        <!-- Finish Sidebar-->
                    </div>
                </div>
            </section>
            <!--End Content-->

        </section>
        <!-- Start Footer -->
        <?php require_once('frontend/temp_front/footer.php'); ?>
        <!-- End Footer -->
    </div>
    <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
    <!--Start js-->
    <?php require_once('frontend/temp_front/js.php'); ?>
    <!--End js-->
</body>

</html>
