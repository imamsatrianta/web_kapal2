<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        $this->template->load('template', 'frontend/home');
    }

    public function visi_misi() {
        $this->template->load('template2', 'frontend/vision_and_mission');
    }

    public function moto() {
        $this->template->load('template2', 'frontend/moto');
    }

    public function history() {
        $this->template->load('template2', 'frontend/history');
    }

    public function achivment() {
        $this->template->load('template3', 'frontend/achivment');
    }

    public function quality_policy() {
        $this->template->load('template2', 'frontend/quality_policy');
    }

    public function our_product() {
        $this->template->load('template3', 'frontend/our_product');
    }

    public function our_photos() {
        $this->template->load('template3', 'frontend/our_photos');
    }

    public function our_videos() {
        $this->template->load('template3', 'frontend/our_videos');
    }
    
    public function career() {
        $this->template->load('template2', 'frontend/career');
    }

    public function contact() {
        $this->template->load('template2', 'frontend/contact');
    }

}
